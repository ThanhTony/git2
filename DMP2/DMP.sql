CREATE TABLE MEDECIN
(
numRPPS BIGINT PRIMARY KEY NOT NULL,
nomM VARCHAR(100),
prenomM VARCHAR(100),
specialite VARCHAR(100),
ville VARCHAR(100),
adresse VARCHAR(250)
)

CREATE TABLE PATIENT
(
    numSS BIGINT PRIMARY KEY NOT NULL,
    nomP VARCHAR(100),
    prenomP VARCHAR(100),
    sexe VARCHAR (100),
    dateNaiss DATE,
    numRPPS BIGINT,
    FOREIGN KEY(numRPPS) REFERENCES MEDECIN(numRPPS)

)

CREATE TABLE CONSULTE
(
numRPPS BIGINT,
numSS BIGINT,
dateSS DATE,
diagnostic VARCHAR(200),
numOrd BIGINT,
FOREIGN KEY(numRPPS) REFERENCES MEDECIN(numRPPS),
FOREIGN KEY(numSS) REFERENCES PATIENT(numSS)

)


INSERT INTO MEDECIN (numRPPS, nomM, prenomM, specialite, ville, adresse)
VALUES
(01231225489, 'Hammouche', 'Chérifa', 'Médecin généraliste', 'Paris', '4 Rue Duc 75018'),
(01248265265, 'Elbaz', 'Jocelyne', 'Médecin généraliste', 'Paris', '4 Place Jacques Froment 75018'),
(01286457874, 'Vidart', 'Anne', 'Médecin généraliste', 'Paris', '152 Rue Legendre 75017'),
(01266675872, 'Fitussi', 'Denis', 'Médecin généraliste', 'Paris', '151 Boulevard Ney 75018'),

(74523562545, 'Houel', 'Orianne', 'Chirurgien-dentiste', 'Paris', '4 Place Jacques Froment 75018'),
(74527683575, 'Laborde', 'Frédéric', 'Chirurgien-dentiste', 'Paris', '28 Rue Clairaut 75017'),
(74514514389, 'Hayat', 'Pierre', 'Chirurgien-dentiste', 'Paris', '95 Avenue Niel 75017'),
(74523562567, 'Ferrandi', 'Mathias', 'Chirurgien-dentiste', 'Paris', '59 Rue Legendre 75017'),

(69045847898, 'Danan', 'Denis', 'Gynécologue-obstétricien', 'Paris', '13 Rue de Tocqueville 75017'),
(69014573512, 'Hommais Loufrani', 'Bettina', 'Gynécologue-obstétricien', 'Paris', '114 Rue Damrémont 75018'),
(69047839678, 'Launois-Lanba', 'Catherine', 'Gynécologue-obstétricien', 'Paris', '54 Rue de Prony 75017'),
(69016837150, 'HARIF', 'Marie', 'Gynécologue-obstétricien', 'Paris', '56 Rue des Batignolles 75017'),
(01467300128, 'Kron', 'Cédric', 'Chirurgien plasticien', 'Paris', '57 Avenue de Villiers 75017'),
(01412345678, 'Hunsinger', 'Vincent', 'Chirurgien plasticien', 'Paris', '7 Rue Bayard 75008'),
(01485296302, 'Santini', 'Christelle', 'Chirurgien plasticien', 'Paris', '89 Rue de la Pompe 75116'),
(01445348674, 'Loriaut', 'Philippe', 'Chirurgien plasticien', 'Paris', '84 Avenue de Wagram 75017'),

(25414258711, 'Krameisen', 'Hervé', 'Ophtalmologiste', 'Paris', '4 Avenue Gourgaud 75017'),
(25478587435, 'Guthmann', 'Ruth', 'Ophtalmologiste', 'Paris', '92 Rue Saint-Lazare 75009'),
(25434524574, 'Albou-Ganem', 'Catherine', 'Ophtalmologiste', 'Paris', '230 Rue du Faubourg Saint-Honoré 75008'),
(25485035440, 'Roussel', 'Françoise', 'Ophtalmologiste', 'Paris', ' 56 Rue des Batignolles 75017'),

(51514230201, 'Delacroix', 'Géraldine', 'Pédiatre', 'Paris', '8 Rue Damrémont 75018'),
(51578808239, 'Dupont', 'Edith', 'Pédiatre', 'Paris', ' 59 Rue Eugène Carrière 75018'),
(51500278673, 'Peyrefitte', ' Florence', 'Pédiatre', 'Paris', '18 Rue Philibert Delorme 75017'),
(51535835645, 'Muhieddine', 'El Mouhebb', 'Pédiatre', 'Paris', '161 Rue Legendre 75017'),

(70024538414, 'Sitruk', 'Laurent', 'Cardiologue', 'Paris', '4 Avenue Gourgaud 75017'),
(70085228068, 'Haddad', 'Samir', 'Cardiologue', 'Paris', '83 Rue Blanche 75009'),
(70014040587, 'Younes', 'Jean', 'Cardiologue', 'Paris', '13 Rue de Trétaigne 75018'),
(70072022084, 'Dahan', 'Michel', 'Cardiologue', 'Paris', '138 Rue de Courcelles 75017'),

(02015220136, 'Pierre', 'Lara', 'Psychiatre', 'Paris', ' 81 Rue de la Mare 75020'),
(02069778207, 'Neveux', 'Nicolas', 'Psychiatre', 'Paris', '9 Rue Troyon 75017'),
(02003504500, 'Schabelman', 'Richard', 'Psychiatre', 'Paris', '101 Rue de Prony 75017 '),
(02072175274, 'Olivier-Perzo', 'Véronique', 'Psychiatre', 'Paris', '9 Avenue de Villiers 75017'),

(12926684304, 'Hershkovitch', 'Didier', 'Dermatologue', 'Paris', '96 Avenue de Saint-Ouen 75018'),
(12987034368, 'Le Bozec', 'Patrick', 'Dermatologue', 'Paris', '88 bis Rue Damrémont 75018'),
(12986157454, 'Salagnac', 'Veronique', 'Dermatologue', 'Paris', '95 Avenue Victor Hugo 75116'),
(12977472034, 'Fouéré', 'Sébastien', 'Dermatologue', 'Paris', '41 Boulevard Henri IV 75004'),


(07575961588, 'Robert', 'Stéphane', 'Anesthésiste', 'Paris', '9 Rue de Quatrefages 75005'),
(07500254012, 'Lavergne', 'Jean-Marc', 'Anesthésiste', 'Paris', '9 Rue de Quatrefages 75005'),
(07512368736, 'Gatt', 'Marie-Thérèse', 'Anesthésiste', 'Paris', '6 Square Jouvenet, 75016'),
(07529435754, 'Puech', 'Michel', 'Anesthésiste', 'Paris', '10 Rue Croix des Petits Champs 75001'),


(12245882201, 'Mourad', 'Kassab', 'Chirurgien orthopédiste', 'Paris', '77 Av. des Champs-Élysées 75008'),
(12289604520, 'Lavergne', 'Jean-Marc', 'Chirurgien orthopédiste', 'Paris', '9 Rue de Quatrefages 75005'),
(12235485287, 'Gatt', 'Marie-Thérèse', 'Chirurgien orthopédiste', 'Paris', '6 Square Jouvenet, 75016'),
(12273255808, 'Puech', 'Michel', 'Chirurgien orthopédiste', 'Paris', '10 Rue Croix des Petits Champs 75001');


INSERT INTO PATIENT (numSS, nomP, prenomP, sexe, dateNaiss, numRPPS)
VALUES
(158236982745123, 'Jean', 'Jaurès', 'Homme', '1985.02.13', 01266675872),
(586325478952148, 'Blanchet', 'Marilaine', 'Femme','1994.12.23', 01286457874),
(789654123369258, 'Marcelin', 'Maurice', 'Homme', '1979.10.02', 01231225489),
(582936714465132, 'Mauge', 'Aurore', 'Femme', '2001.06.29', 01248265265),
(783691294249753, 'Bernard', 'Jean-Eudes', 'Homme', '1924.10.18', 01266675872),
(357951258739149, 'Xu', 'Jack', 'Homme', '1999.01.25', 01266675872),
(780690580492470, 'Graulle', 'Matthieu', 'Homme', '1999.06.30', 01231225489),
(852357961031589, 'Limam', 'Inès', 'Femme', '2001.02.18', 01248265265);

INSERT INTO CONSULTE (numRPPS, numSS, dateSS, diagnostic, numOrd)
VALUES
(01248265265, 582936714465132, '2019.02.28', 'rhume', 001),
(25478587435, 780690580492470, '2019.04.11', 'myopie', 002),
(70072022084, 158236982745123, '2019.01.01', 'souffle cardiaque',003),
(02069778207, 586325478952148, '2019.03.30', 'troubles bipolaires',004),
(12289604520, 783691294249753, '2019.04.02', 'Abcès péri-apical', 005);
